const port_ecoute_node = require("./settings.js").port_ecoute_serveur_node;
const host_backend_java = require("./settings.js").host_backend_java;

const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const IoManager = require("./services/ioManager.js");
const ioManager = new IoManager(
  new Server(server, {
    cors: {
      origin: host_backend_java,
      allowedHeaders: ["my-custom-header"],
    },
  })
);

app.use("/chat", express.static('www'))

ioManager.init();

server.listen(3001, () => {
    console.log('listening on *:3001');
});
