// private services
const UserMapper = require('./userMapper.js');
const GameMapper = require('./gameMapper.js');
const UserManager = require('./io/userManager.js');
const MsgManager = require('./io/msgManager.js');
const GameManager = require('./io/gameManager.js')

const userMapper = new UserMapper();
const gameMapper = new GameMapper(userMapper);

const userManager = new UserManager(userMapper);
const msgManager = new MsgManager(userMapper);

let gameManager = null;

class IoManager {

    io = null;

    constructor(io) {
        this.io = io;
        gameManager = new GameManager(userMapper, gameMapper, this);
    }

    init() {
        this.io.on('connection', function (socket) {
            socket.on("data-card-app", function (data) {
                switch (data.type) {
                    case "user":
                        console.log("...User data...");
                        userManager.dispatch(socket, data.action, data.message);
                        break;
                    case "msg":
                        console.log("...Msg data...");
                        msgManager.dispatch(socket, data.action, data.message);
                        break;
                    case "game":
                        console.log("...Game data...");
                        gameManager.dispatch(socket, data.action, data.message);
                        break;
                    default:
                        console.log("...Incorrect data...");
                }

            });

            // impossible de mettre dans le onAny
            socket.on('disconnect', () => {
                if (userMapper.check_user_connection(socket.login)) {
                    userMapper.remove_user(socket.login);
                    console.log(socket.login + " left.");
                    socket.broadcast.emit("disconnect-event", socket.login);
                }
            });
        });
    }

    send_message(socket_id, type, data) {
        this.io.to(socket_id).emit(type, data);
    }

}
module.exports = IoManager;