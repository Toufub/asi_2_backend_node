const end_point_card = require("../../settings.js").host_backend_java + require("../../settings.js").end_point_get_card_by_id;

const request = require("request");

class GameMaster {
  joueur_A = null;
  joueur_B = null;
  gameMapper = null;
  actual_player = null;
  io = null;

  constructor(io, joueur_A, joueur_B, gameMapper) {
    this.io = io;
    this.joueur_A = joueur_A;
    this.joueur_B = joueur_B;
    this.actual_player = null;
    this.gameMapper = gameMapper;
    this.initialise_game();
  }

  initialise_game() {
    this.io.send_message(this.joueur_A.socket, "game_initialize", this.joueur_B.user);
    this.io.send_message(this.joueur_B.socket, "game_initialize", this.joueur_A.user);
  }

  get_joueur_by_login(login) {
    if (this.joueur_A.user === login) {
      return this.joueur_A;
    } else if (this.joueur_B.user === login) {
      return this.joueur_B;
    } else {
      return false;
    }
  }

  set_card(login, cards) {
    let joueur = this.get_joueur_by_login(login);
    if (joueur && joueur.ready === false) {
      joueur.ready = true;
      joueur.cards = cards;
      this.io.send_message(joueur.socket, "card_receive", "ok");
      if (this.joueurs_ready()) {
        this.joueur_A.action_points = 0;
        this.joueur_B.action_points = 0;
        this.io.send_message(this.joueur_A.socket, "game_start", { ok: "ok", cardsOpposant: this.joueur_B.cards });
        this.io.send_message(this.joueur_B.socket, "game_start", { ok: "ok", cardsOpposant: this.joueur_A.cards });
        this.get_joueur_to_start();
        this.send_action_points();
      }
    }
  }

  joueurs_ready() {
    return this.joueur_B.ready && this.joueur_A.ready;
  }

  get_card_by_id(id) {
    request(end_point_card + id, { json: true }, (err, res, body) => {
      if (err) {
        return console.log(err);
      }
      return res;
    });
  }

  get_joueur_to_start() {
    let choice = Math.floor(Math.random() * 2);
    if (choice === 0) {
      this.actual_player = this.joueur_A;
    } else {
      this.actual_player = this.joueur_B;
    }
    this.io.send_message(this.joueur_A.socket, "game_turn", this.actual_player.user);
    this.io.send_message(this.joueur_B.socket, "game_turn", this.actual_player.user);
  }

  end_turn(login) {
    if (this.actual_player.user === login) {
      this.change_player_turn();
    }
  }

  attack(login, card_src, card_dest) {
    if (login === this.actual_player.user) {
      if (this.verify_card(this.actual_player, card_src) && this.verify_card(this.get_next_player(), card_dest)) {
        if (card_src.energy < this.actual_player.action_points) {
            var index = this.get_next_player().cards.findIndex(x => x.id === card_dest?.id);
            card_dest.hp = card_dest.hp - card_src.attack;
          if (card_dest.hp < 0) {
            card_dest.hp = 0;
          }
          this.get_next_player().cards[index].hp = card_dest.hp;
          this.actual_player.action_points -= card_src.energy;
          this.io.send_message(this.actual_player.socket, "action_points", this.actual_player.action_points);
          this.io.send_message(this.joueur_A.socket, "new-card-hp", { health: card_dest.hp, id: card_dest.id });
          this.io.send_message(this.joueur_B.socket, "new-card-hp", { health: card_dest.hp, id: card_dest.id });
          let winner = this.get_eventual_winner();
          if (winner === false) {
            // if(this.actual_player.action_points === 0) {

            this.change_player_turn();
            // } else {
            //     this.io.send_message(this.actual_player.socket, "continue", "ok");
            // }
          } else {
            this.close_game(winner);
          }
        } else {
          this.io.send_message(this.actual_player.socket, "action_points_insuffisant");
        }
      }
    }
  }

  change_player_turn() {
    this.actual_player = this.get_next_player();
    this.io.send_message(this.joueur_B.socket, "game_turn", this.actual_player.user);
    this.io.send_message(this.joueur_A.socket, "game_turn", this.actual_player.user);
    this.send_action_points();
  }

  send_action_points() {
    this.actual_player.action_points += 10;
    this.io.send_message(this.actual_player.socket, "action_points", this.actual_player.action_points);
    this.io.send_message(this.actual_player.socket, "play-time", "ok");
  }

  verify_card(joueur, card) {
    if (joueur.cards.find((x) => x.id === card?.id)) return true;

    return false;
  }

  get_eventual_winner() {
    let winner = false;
    if (this.player_has_lost(this.joueur_B)) {
      winner = this.joueur_A;
    } else if (this.player_has_lost(this.joueur_A)) {
      winner = this.joueur_B;
    }
    return winner;
  }

  player_has_lost(joueur) {
    if (joueur.cards.some((x) => x.hp > 0)) return false;
    return true;
  }

  get_next_player() {
    if (this.actual_player === this.joueur_A) {
      return this.joueur_B;
    } else if (this.actual_player === this.joueur_B) {
      return this.joueur_A;
    } else {
      return false;
    }
  }

  close_game(winner) {
    this.io.send_message(this.joueur_B.socket, "game-winner", winner.user);
    this.io.send_message(this.joueur_A.socket, "game-winner", winner.user);
    this.gameMapper.close_game(winner.user);
    // future envoi côté back pour enregistrer les duels
  }
}
module.exports = GameMaster;
