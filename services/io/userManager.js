class UserManager {
    userMapper = null;

    constructor(userMapper) {
        this.userMapper = userMapper;
    }

    dispatch(socket, action, data) {
        switch(action) {
            case "send-login":
                this.send_login(socket, data);
                break;
            case "fix-id":
                this.fix_id(socket, data);
                break;
            case "disconnect":
                this.disconnect(socket,data);
                break;
            default:
                console.log("...Error...");
        }
    }


    disconnect(socket,login){
        if (this.userMapper.check_user_connection(login)) {
            this.userMapper.remove_user(login);
            console.log(login + " left.");
            socket.broadcast.emit("disconnect-event", login);
        }
    }

    /**
     *
     * Initialisation de l'utilisateur sur le Socket
     * (Authentification)
     *
     */

    send_login(socket, login) {
        if(this.userMapper.add_user(login, socket.id)) {
            socket.emit("welcome-message", this.userMapper.get_all_logins_connected());
            socket.broadcast.emit("connect-event", login);
            console.log(login + " join.");
        } else {
            socket.emit("forbidden", "already-use");
            socket.disconnect();
        }
    }

    /**
     *
     * Réactualise l'ID du socket par rapport à son login
     *
     */

    fix_id(socket, login) {
        if(this.userMapper.fix_socket_id(login, socket.id)) {
            socket.login = login;
            console.log(login + " fixed.");
        } else {
            socket.emit("forbidden", "Not connected !");
            socket.disconnect();
        }
    }


}

module.exports = UserManager;