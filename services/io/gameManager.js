const GameMaster = require('./gameMaster.js');

class GameManager {

    userMapper = null;
    gameMapper = null;
    io = null;

    constructor(userMapper, gameMapper, io) {
        this.userMapper = userMapper;
        this.gameMapper = gameMapper;
        this.io = io;
    }

    dispatch(socket, action, data) {
        switch(action) {
            case "create-game":
                this.createGame(socket, data);
                break;
            case "cancel-game":
                this.cancelGame(socket, data);
                break;
            case "accept-game":
                this.acceptGame(socket, data);
                break;
            case "deny-game":
                this.denyGame(socket, data);
                break;
            case "cards":
                this.receiveCards(socket, data);
                break;
            case "endturn":
                this.endTurn(socket, data);
                break;
            case "attack":
                this.attack(socket, data);
                break;
            default:
                console.log("...Error...");
        }
    }

    createGame(socket, other_login) {
        let user_id = this.userMapper.get_user_login(socket.id);
        let game = this.gameMapper.initialise_gaming_room(user_id, other_login);
        if(game) {
            console.log(user_id + " wants to fight against " + other_login + " !",this.userMapper.get_socket_id(other_login));
            socket.to(this.userMapper.get_socket_id(other_login)).emit("game-ask", user_id);
            socket.emit("create-game-confirm", {ok:"ok",opponent: other_login});
        }
    }

    cancelGame(socket, other_login) {
        let user_id = this.userMapper.get_user_login(socket.id);
        if(this.gameMapper.cancel_gaming_room(user_id, other_login)) {
            console.log(user_id + " has cancel his request to " + other_login);
            socket.to(this.userMapper.get_socket_id(other_login)).emit("game-cancel", user_id);
            socket.emit("cancel-game-confirm", {ok:"ok",opponent: other_login});
        }
    }

    acceptGame(socket, other_login) {
        let user_id = this.userMapper.get_user_login(socket.id);
        if(this.gameMapper.confirm_gaming_room(other_login, user_id)) {
            console.log(user_id + " accept to fight against " + other_login + " !");
            socket.to(this.userMapper.get_socket_id(other_login)).emit("game-confirm", user_id);
            socket.emit("accept-game-confirm", "ok");

            this.gameMapper.add_game_master(user_id, other_login, new GameMaster(this.io, {user: user_id , socket: socket.id, ready: false, cards: null},
                {user: other_login, socket: this.userMapper.get_socket_id(other_login), ready: false, cards: null},this.gameMapper));
        }
    }

    denyGame(socket, other_login) {
        let user_id = this.userMapper.get_user_login(socket.id);
        if(this.gameMapper.deny_gaming_room(other_login, user_id)) {
            console.log(user_id + " refuse to fight against " + other_login + " !");
            socket.to(this.userMapper.get_socket_id(other_login)).emit("game-deny", user_id);
            socket.emit("deny-game-confirm", "ok");
        }
    }

    receiveCards(socket, cards) {
        let user_id = this.userMapper.get_user_login(socket.id);
        let game = this.gameMapper.get_game(user_id);
        if(game && game.game_master) {
            game.game_master.set_card(user_id, cards);
        }
    }

    endTurn(socket, data) {
        let user_id = this.userMapper.get_user_login(socket.id);
        let game = this.gameMapper.get_game(user_id);
        if(game && game.game_master) {
            game.game_master.end_turn(user_id);
        }
    }

    attack(socket, data) {
        let user_id = this.userMapper.get_user_login(socket.id);
        let game = this.gameMapper.get_game(user_id);
        if(game && game.game_master) {
            game.game_master.attack(user_id, data.card_src, data.card_dest);
        }
    }
}

module.exports = GameManager;