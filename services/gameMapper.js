class GameMapper {

    user_mapper = null;
    gaming_rooms = [];

    constructor(user_mapper) {
        this.user_mapper = user_mapper;
    }

    initialise_gaming_room(initializer, dest) {
        let gaming_room = false;
        if (this.user_mapper.check_user_connection(dest)) {
            if (this.get_game(initializer) === false) {
                this.gaming_rooms.push({
                    initializer: initializer,
                    dest: dest,
                    game_master: null,
                    accept: false,
                });
            }
            gaming_room = this.get_game(initializer,dest);
        }
        return gaming_room;
    }

    get_game(joueur_A, joueur_B = null) {
        let index = this.get_game_index(joueur_A, joueur_B);
        if (index === -1) {
            return false;
        } else {
            return this.gaming_rooms[index];
        }
    }

    add_game_master(joueur_A, joueur_B, master) {
        this.get_game(joueur_A, joueur_B).game_master = master;
    }

    get_game_index(joueur_A, joueur_B = null) {
        if(joueur_B) {
            return this.gaming_rooms.findIndex(object => {
                return (object.initializer === joueur_B && object.dest === joueur_A) ||
                    (object.initializer === joueur_A && object.dest === joueur_B);
            });
        } else {
            return this.gaming_rooms.findIndex(object => {
                return object.initializer === joueur_A || object.dest === joueur_A;
            });
        }
    }

    close_game(winner) {
        let index = this.get_game_index(winner);
        if(index) {
            this.gaming_rooms.splice(index, 1);
            return true;
        } else {
            return false;
        }
    }

    confirm_gaming_room(initializer, dest) {
        let confirm = false;
        let index = this.get_game_index(initializer, dest);
        if(index >= 0) {
            if(this.gaming_rooms[index].accept === false) {
                this.gaming_rooms[index].accept = true;
                confirm = true;
            }
        }
        return confirm;
    }

    deny_gaming_room(initializer, dest) {
        let confirm = false;
        let index = this.get_game_index(initializer, dest);
        if(index >= 0) {
            if(this.gaming_rooms[index].accept === false) {
                this.gaming_rooms.splice(index, 1);
                confirm = true;
            }
        }
        return confirm;
    }

    cancel_gaming_room(initializer, dest) {
        let confirm = false;
        let index = this.get_game_index(initializer, dest)
        if(index >= 0) {
            this.gaming_rooms.splice(index, 1);
            confirm = true;
        }
        return confirm;
    }

}

module.exports = GameMapper;