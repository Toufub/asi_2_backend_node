class UserMapper {

    logins_connected = [];
    logins_sessions = [];
    tchatting_rooms = [];

    constructor() {

    }

    get_all_logins_connected() {
        return this.logins_connected;
    }

    fix_socket_id(login, id) {
        let val_retour = false;
        if(this.check_user_connection(login) === true) {
            val_retour = true;
            let index = this.logins_sessions.findIndex(elem => elem.login === login);
            this.logins_sessions[index].session = id;
        }
        return val_retour;
    }

    get_socket_id(login) {
        return this.logins_sessions.find(obj => {
            return obj.login === login;
        })?.socket;
    }

    get_user_login(socket) {
        return this.logins_sessions.find(obj => {
            return obj.socket === socket;
        })?.login;
    }

    check_user_connection(login) {
        return this.logins_connected.includes(login);
    }

    add_user(login,socketId) {
        let success = false;
        if(this.logins_connected.includes(login) === false) {
            this.logins_connected.push(login);
            this.logins_sessions.push({login: login,socket:socketId});
            this.tchatting_rooms[login] = false;
            success = true;
        }
        return success;
    }

    remove_user(login) {
        const found = this.logins_sessions.findIndex(element => element?.login === login);
        this.logins_connected.splice(this.logins_connected.indexOf(login), 1);
        delete this.logins_sessions[found];
        delete this.tchatting_rooms[login];
    }

    create_tchatting_room(login, partner) {
        let is_tchatting = false;
        if(partner !== "" && this.check_user_connection(partner)) {
            this.tchatting_rooms[login] = partner;
            is_tchatting = true;
        } else {
            this.tchatting_rooms[login] = false;
        }
        return is_tchatting;
    }

    get_tchatting_partner(login) {
        let partner = false;
        if(this.tchatting_rooms[login] !== false) {
            if(this.check_user_connection(this.tchatting_rooms[login])) {
                partner = this.tchatting_rooms[login];
            } else {
                this.tchatting_rooms[login] = false;
            }
        }
        return partner;
    }
}

module.exports = UserMapper;