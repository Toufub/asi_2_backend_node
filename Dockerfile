FROM node:18.12

ENV NODE_ENV=production

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/

RUN npm install --omit=dev --quiet
COPY . /usr/src/app

EXPOSE 8080
CMD ["npm", "start"]


