class Settings {
    static port_ecoute_serveur_node = 3001; // port d'écoute du NodeJS
    static host_backend_java = "http://localhost:3000";

    static end_point_get_card_by_id = "/à renseigner/"; // ne pas intégrer l'host du backend java et
                                                        // penser à ajouter le '/' au début et à la fin
}
module.exports = Settings;