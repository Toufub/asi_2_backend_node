
# Atelier 2 - Rapport
PELLARIN/MOOTE/CAMBRAY/BEAUD

## Phase A
Node.Js se base sur le moteur Javascript pour réaliser des opérations en backend. 
#### Avantages 
- Il est particulièrement léger ce qui permet une mise à l'échelle plus facile. 
- NodeJS utilise un système de requêtes asyncrones ce qui permet de traiter des milliers de requêtes en parallels.
- Ce système rend nodejs très performant

#### Inconvénients
- Il est plus difficile de s'interfacer avec une base de données relationnelles (comparé à Java/Django..)
- NodeJS n'est pas bon avec les calculs lourds
- La gestion des dépendances peut vite poser problèmes et de nombreuses failles sont régulièrement trouvées

## Configuration

Le fichier de configuration est disponible à la racine : `settings.js`.

## Communication avec le socket IO

Tout d'abord, l'entièreté des messages à envoyer au socket auront pour topic : `data-card-app`.
La différenciation se fait dans le contenu de l'objet JSON envoyé.

### User

La partie User est très simple et permet à un utilisateur de s'authentifier
côté backend nodeJS, cela sert pour le tchat et pour que tous les utilisateurs
puissent avoir une idée de qui est en ligne et quand.

#### Authentification

Pour s'authentifier envoyer un object JSON comme suit au socket (dès qu'un utilisateur arrive sur l'application)
```
{
    type: "user",
    action: "send-login",
    message: ID_de_l_utilisateur
}
```

#### Messages envoyés par le socket

* `welcome-message` avec pour data un tableau regroupant l'ensemble des utilisateurs (ID). Ce message est envoyé si votre authentification est réussie
* `forbidden` avec pour data `already-use` si l'ID de l'utilisateur est déjà connecté au back-end NODE => conflit
* `connect_event` broadcast contenant l'ID de l'utilisateur qui s'est connecté, pour informer les autres utilisateurs de la connexion.

### Messagerie Instantanée

La partie messagerie permet de gérer les messages en broadcast et les messages privés. Elle nécessite d'être authentifié, cf. partie précédente.

#### Envoyer un message

Pour envoyer un message envoyer ce format JSON :
```
{
    type: "msg",
    action: "message",
    message: contenu_du_message
}
```

#### Envoyer un message privé.

Pour envoyer un message privé, il faut envoyer le même objet JSON cité plus haut.
Cependant, pour informer le backend NodeJS du besoin de parler à un utilisateur particulier, envoyer cet objet JSON en amont.
Cela crééra une "tchatting_room" qui permettra à l'utilisateur d'envoyer des message privé en continu.
```
{
    type: "msg",
    action: "tchatting-with",
    message: ID_de_l_utilisateur_qui_recevra_les_messages
}
```
Si le contenu de `message` dans l'objet JSON est vide le serveur supprimera la tchatting_room. Pareil si l'ID de l'utilisateur 
demandé est non-connecté.

#### Messages envoyés par le socket

Pour cette partie, le socket n'envoie que des messages sous le topic : `message` aux
utilisateurs concernés et se charge du formattage des message.
Il faut simplement écouter les données sur `message`.

### Game

La partie jeu est la plus complexe.

#### Défier quelqu'un

Pour défier quelqu'un envoyer l'objet JSON suivant
(impossible d'envoyer deux défis en même temps) :
```
{
    type: "game",
    action: "create-game",
    message: ID_de_l_utilisateur_défié
}
```

#### Annuler sa demande de défi

Pour annuler sa proposition de défi envoyer l'objet JSON suivant :
```
{
    type: "game",
    action: "cancel-game",
    message: ID_de_l_utilisateur_défié
}
```

#### Accepter une demande de défi

Pour accepter une proposition de défi envoyer l'objet JSON suivant :
```
{
    type: "game",
    action: "accept-game",
    message: ID_de_l_utilisateur_qui_a_défié
}
```

#### Refuser une demande de défi

Pour refuser une proposition de défi envoyer l'objet JSON suivant :
```
{
    type: "game",
    action: "deny-game",
    message: ID_de_l_utilisateur_qui_a_défié
}
```

#### Envoyer la liste de ses cartes sélectionnées pour le défi

Si la proposition de défi est accepté le combat est lancé et le joueur doit envoyer ses cartes :
```
{
    type: "game",
    action: "cards",
    message: tableau_regroupant_tous_les_ID_de_cards_selectionnées
}
```

#### Terminer son tour

Pour terminer son tour, envoyer le JSON suivant :
```
{
    type: "game",
    action: "endturn",
    message: inutile_ici
}
```

#### Lancer une attaque

Pour lancer une attaque, envoyer le JSON suivant :
```
{
    type: "game",
    action: "attack",
    message: {
        card_src: ID_card_qui_attaque,
        card_dest: ID_card_qui_recoit_l_attaque
    }
}
```

#### Messages envoyés par le socket pour la création des gaming-rooms.

* `create-game-confirm`: Informe l'utilisateur que son défi a bien été envoyé (data = "ok").
* `game-ask`: Informe l'utilisateur qu'un défie lui est lancé. (data=ID_utilisateur_qui_défie)
* `cancel-game-confirme`: Informe l'utilisateur que son défi a bien été annulé (data="ok").
* `game-cancel`: Informe l'utilisateur que le défi qui lui est lancé est annulé. (data=ID_utilisateur_qui_défiait)
* `game-deny`: Informe l'utilisateur que son défi a été refusé.
* `deny-game-confirm`: Confirme à l'utilisateur que son refus a bien été reçu.

#### Messages envoyés par le socket pendant une partie (par le Maître du jeu)

* `game_initialize`: Informe l'utilisateur que sa partie est bien lancée. (data=ID_utilisateur_affronté)
* `card_receive`: Informe l'utilisateur que ses cartes ont bien été reçues. (data="ok")
* `game_start`: Informe l'utilisateur que la partie est lancée. (data="ok")
* `actions_point`: Envoie la nouvelle valeur de points d'actions dont l'utilisateur dispose. (data=nb_points_actions)
* `game_turn`: Envoie l'ID de l'utilisateur dont le tour démarre. (data=ID_utilisateur_qui_joue)
* `play-time`: Informe l'utilisateur qu'il peut jouer. (data="ok")
* `new-card-hp`: Envoie la nouvelle valeur de vie de la carte attaquée. (data=`{health:vie_restante, id:id_card}`)
* `continue`: Informe l'utilisateur qu'il peut continuer de jouer. (data="ok")
* `game_winner`: Informe l'utilisateur du gagnant de la partie lorsqu'elle est terminée (détection automatique par le backend NodeJS). (data=ID_utilisateur_gagnant)